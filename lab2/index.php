<?php

use edu\itas\db\Database as Database;
use edu\itas\housePhotos\models\User as User;

use \PDO as PDO;

/**
 * User: croftd
 * Date: 4/17/2017
 * 
 * Index file for REW301 lab2
 */

// autoload function
function __autoload($class) {
    // convert namespace to full file path
    $class = '' . str_replace('\\', '/', $class) . '.php';

    // Note if you keep classes all in a subfolder you can
    // replace '' with 'classes' etc..   \
    require_once($class);

}

$user = new User('croftd', 'itas1234', 'Dave', 'Croft');
$user->save();

$pdo = Database::connect();
$statement = $pdo->query('SELECT * FROM user');

echo "Results from the User table with FETCH_INTO:<br>";

// croftd: Note FETCH_INTO only works if User fields are public!
// That is why they are set to public in the User class
//
$statement->setFetchMode(PDO::FETCH_INTO, new User);
foreach($statement as $user)
{
   echo "User: [" . $user->getId() . "]: " . $user->getName() . '<br>';
}

//echo "Results from the User table with FETCH_CLASS:<br>";
// croftd: Issues with this
//$rows = $statement->fetchAll(PDO::FETCH_CLASS, 'User');
//var_dump($rows);
//foreach ($rows as $row) {
//        echo "<br>User: " . $row->getName() . "<br>";
//        var_dump($row);
//}

// To complete the lab, test that this works:

//$user->setFirstName("Bob");
//$user->update();

// print out this updated user

//$id = $user->getId();

// get a new User object from the db based upon $id
//$user2 = User::find($id);

//$user->delete();
// check that $id is no longer in the database

//$users = User::fetchAll();

// loop through and display all these users
// foreach ($users as $user) { // etc... }
