<?php
namespace edu\itas\housePhotos\models;

use edu\itas\db\Database as Database;

/**
 * Created by PhpStorm.
 * User: croftd
 * Date: 4/18/2017
 *
 * NOTE: This class won't work AS-IS
 */

class User
{
    public $id;
    public $username;
    public $password;
    public $firstName;
    public $lastName;


    // $user = new User('croftd', 'itas1234', 'Dave', 'Croft');

    //public function __construct()
    public function getName() {
        return $this->firstName . " " . $this->lastName;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * croftd: here is a start for one of the methods required for the ActiveRecord interface.
     */
    public function save()
    {
        $pdo = Database::connect(); // static method call

        // Formulate a SQL statement to create a new `user` record
        $sql = "INSERT INTO `user` (`username`, `password`, `firstName`, `lastName`) "
            . "VALUES (:username, :password, :firstName, :lastName)";


        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':firstName', $this->firstName);
        $stmt->bindParam(':lastName', $this->lastName);

        // execute the query
        if ($result = $stmt->execute()) {
            // Set the memory object's id
            $this->id = $pdo->lastInsertId();
            $pdo = null;
            return true;
        }
        $pdo = null;
        return false;
    }


}