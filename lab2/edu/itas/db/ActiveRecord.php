<?php
namespace edu\itas\db;
/**
 * Created by PhpStorm.
 * User: croftd
 * Date: 4/18/2017
 * Time: 11:42 AM
 *
 * Enforce a minimal level of database functionality in implementing classes
 *
 * @author dutchukm, modified by DC for REW301 and PHP5 versions
 */
interface ActiveRecord {

    /**
     * Save the current object as a new record into the database.
     * Return the auto_incremented ID generated by the database.
     *
     * @return int
     */
    public function save();

    /**
     * Delete (from the database) the record corresponding to
     * the current memory object, and set the memory object
     * fields to null
     */
    public function delete();

    /**
     * Save the current object to an existing record in the database.
     */
    public function update();

    /**
     * Fetch the record from the database whose ID is $id and hydrate the current
     * memory object's fields with that data.
     *
     * @param int $id
     * @return ActiveRecord Return a hydrated object or null if no match
     */
    public static function find($id);

    /**
     * Fetch, from the database, all records and return them as a
     * (possibly empty) array of objects.
     *
     * @return array An array of ActiveRecord objects
     */
    public static function findAll(): array;

}