<?php
namespace edu\itas\db;

use \PDO;
use \PDOException;

/**
 * Created by PhpStorm.
 * User: croftd
 * code adopted from dutchukm 186 example, modified to be Singleton with only one PDO copy
 * Date: 4/18/2017
 * Time: 11:35 AM
 */
class Database {

    /** Constructor */
    private function __construct() {}  // Prevents instantiation

    /**
     * Open a connection to the database.
     *
     * @return PDO connection object
     */
    public static function connect() {
        try {
            return new PDO("mysql:host=localhost;dbname=housePhotos", "root", "");
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }
}