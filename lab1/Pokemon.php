<?php

abstract class Pokemon{
	
	private $weight;
	private $HP;
	private $type;

	
	function getWeight(){
		return $this->weight;
	}
	function getHP(){
		return $this->HP;
	}	
	function getType(){
		return $this->type;
	}




	function setLongitude($longitude){
		$this->longitude = $longitude;
	}

	function setLatitude($latitude){
		$this->latitude = $latitude;
	}

	protected function __construct($name, $image, $weight, $HP, $latitude, $longitude, $type){
		$this->name = $name;
		$this->image = $image;
		$this->weight = $weight;
		$this->HP = $HP;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
		$this->type = $type;
	}

	public function __toString(){
		echo "Image: " . $image;
		echo "name " . $name;
		echo "Latitude " . $latitude;
		echo "longitude " . $longitude;
		echo "weight: " . $weight;
		echo "HP: " . $HP;
		echo "type " . $type;
	}

	function attack(Pokemon $other){
		//echo "Base class, no attack";
		$other->HP = $other->HP - $this->getDamage();
	}
	
	public abstract function getDamage();
		
	

}