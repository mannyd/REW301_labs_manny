<?php

interface Flyer {

	function getDuration();
	function takeOff();
	function land();
	function getFlying();
	function getSpeed();
	function getDirection();

}