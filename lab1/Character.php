<?php

abstract class Character {
	private $name;
	private $image;
	private $latitude;
	private $longitude;

	function getName(){
		return $this->name;
	}
	function getImage(){
		return $this->image;
	}
	function getLatitude(){
		return $this->latitude;
	}
	function getlongitude(){
		return $this->longitude;
	}

	function getJSON(){
		return "JSON data";
	}
}